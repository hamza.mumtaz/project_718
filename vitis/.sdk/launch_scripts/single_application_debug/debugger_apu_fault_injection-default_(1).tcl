connect -url tcp:127.0.0.1:3121
source C:/Xilinx_2019_2/Vitis/2019.2/scripts/vitis/util/zynqmp_utils.tcl
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw C:/Hamza/Mixed_Criticality/workspace/vitis/Base_Zynq_MPSoC_wrapper_2/export/Base_Zynq_MPSoC_wrapper_2/hw/Base_Zynq_MPSoC_wrapper.xsa -mem-ranges [list {0x80000000 0xbfffffff} {0x400000000 0x5ffffffff} {0x1000000000 0x7fffffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~ "*A53*#0"}
rst -processor
dow C:/Hamza/Mixed_Criticality/workspace/vitis/apu_fault_injection/Debug/apu_fault_injection.elf
configparams force-mem-access 0
bpadd -addr &main
