/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * apuFaultInjection.c: simple test application injecting isolation faults
 * using reads and writes from the APU into RPU/APU Secure and Non-Secure memory
 * address locations
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_io.h"
#include "xil_cache.h"
#include "xil_exception.h"
#include "apuFaultInjection.h"


static int 	SetupInterruptSystem(void);
void 		SAbort_SyncIntHandler(int);
void 		SAbort_SErrorAbortIntHandler(int);

static void readReg(char registerName[30], u32 registerAddress);
static void writeReg(char registerName[30], u32 registerAddress, u32 regVal);

bool 		exceptionDetected = false;



int main()
{
    init_platform();
    SetupInterruptSystem(); //register custom interrupt handlers in this application

    // Disable caches here for demonstration purposes.  If enabled all write violations look like read violations
    // due to the cache read before a write transaction.
    Xil_DCacheDisable(); //disable data cache
    Xil_ICacheDisable(); //disable instruction cache


	print("---Starting Fault Injection Test (Running on the APU)---\n\n\r");
	print("   (S)=Secure, (NS)=Non-Secure, (ND)=Not-Defined\n\r");

	print("   Memories\n\r");
	print("   \tRPU_OCM_S_BASE                : OCM Secure Memory Base Address in RPU Sub-System\n\r");
	print("   \tRPU_OCM_NS_SHARED_BASE        : OCM Non-Secure Memory Base Address in both RPU and APU Sub-Systems\n\r");
	print("   \tRPU_ATCM_S_BASE               : R5 TCM Bank A Secure Memory Base Address in RPU Sub-System\n\r");
	print("   \tRPU_DDR_LOW_S_BASE            : DDR Secure Memory Base Address in RPU Sub-System\n\r");
	print("   \tRPU_DDR_LOW_NS_SHARED_BASE    : DDR Non-Secure Memory Base Address in both RPU and APU Sub-Systems\n\r");
	print("   \tAPU_OCM_NS_SHARED_BASE        : OCM Non-Secure Memory Base Address in both APU and RPU Sub-Systems\n\r");
	print("   \tAPU_DDR_LOW_NS_BASE           : DDR Non-Secure Memory Base Address in APU Sub-System\n\r");
	print("   \tAPU_DDR_LOW_NS_SHARED_BASE    : DDR Non-Secure Memory Base Address in Both APU and RPU Sub-Systems\n\r");
	print("   \tUNDEFINED_DDR_MEMORY_BASE     : Memory Base Address Not Defined in Any Sub-System\n\r");

	print("   Peripherals\n\r");
	print("   \tAPU_UART0_NS_START            : Non-Secure UART0 in APU Sub-System\n\r");
	print("   \tAPU_SWDT0_NS_START            : Non-Secure UART0 in APU Sub-System\n\r");
	print("   \tAPU_TTC0_NS_START             : Non-Secure UART0 in APU Sub-System\n\r");
	print("   \tAPU_UART0_NS_START            : Non-Secure UART0 in APU Sub-System\n\r");
	print("   \tSHARED_GPIO_NS_START          : Shared Non-Secure GPIO\n\r");
	print("   \tRPU_UART1_S_START             : Secure UART1 in RPU Sub-System\n\r");
	print("   \tRPU_SWDT1_S_START             : Secure SWDT1 in RPU Sub-System\n\r");
	print("   \tRPU_TTC1_S_START              : Secure TTC1 in RPU Sub-System\n\r");
	print("   \tRPU_I2C1_S_START              : Secure I2C1 in RPU Sub-System\n\r");


	usleep(10*DELAY_COUNT);


// Memory Checks (Read / Modify / Write / Read)

	print("\n\n   Read/Write Of Various Memories\n\r");
		print("\n\t Read/Write Of RPU(S) Memory\n\r");
			readReg("RPU_OCM_S_BASE",  RPU_OCM_S_BASE);
		    writeReg("RPU_OCM_S_BASE", RPU_OCM_S_BASE, 0xDEADBEEF);

			readReg("RPU_DDR_LOW_S_BASE",  RPU_DDR_LOW_S_BASE);
		    writeReg("RPU_DDR_LOW_S_BASE", RPU_DDR_LOW_S_BASE, 0xDEADBEEF);

			readReg("RPU_ATCM_S_BASE",  RPU_ATCM_S_BASE);
		    writeReg("RPU_ATCM_S_BASE", RPU_ATCM_S_BASE, 0xDEADBEEF);

		print("\n\t Read/Write Of RPU(NS) Memory\n\r");
			readReg("RPU_OCM_NS_SHARED_BASE",  RPU_OCM_NS_SHARED_BASE);
			writeReg("RPU_OCM_NS_SHARED_BASE", RPU_OCM_NS_SHARED_BASE, 0xDEADBEEF);

			readReg("RPU_DDR_LOW_NS_SHARED_BASE",  RPU_DDR_LOW_NS_SHARED_BASE);
			writeReg("RPU_DDR_LOW_NS_SHARED_BASE", RPU_DDR_LOW_NS_SHARED_BASE, 0xDEADBEEF);

		print("\n\t Read/Write Of APU(S) Memory\n\r");

			print("\t\t---This combination does not exist\n\r");
			print("\t\t---APU has no (S)ecure memory\n\r");
			usleep(DELAY_COUNT);

		print("\n\t Read/Write Of APU(NS)\n\r");
			readReg("APU_OCM_NS_SHARED_BASE",  APU_OCM_NS_SHARED_BASE);
			writeReg("APU_OCM_NS_SHARED_BASE", APU_OCM_NS_SHARED_BASE, 0xDEADBEEF);

			readReg("APU_DDR_LOW_NS_BASE",  APU_DDR_LOW_NS_BASE);
			//writeReg("APU_DDR_LOW_NS_BASE", APU_DDR_LOW_NS_BASE, 0xDEADBEEF);
			print("\t\t Writing APU_DDR_LOW_NS_BASE\t\t...  Skipped to avoid memory collision!\n\r");


			readReg("APU_DDR_LOW_NS_SHARED_BASE",  APU_DDR_LOW_NS_SHARED_BASE);
			writeReg("APU_DDR_LOW_NS_SHARED_BASE", APU_DDR_LOW_NS_SHARED_BASE, 0xDEADBEEF);

		print("\n\t Read/Write Of Undefined (ND) Memory\n\r");
			readReg("UNDEFINED_DDR_MEMORY_BASE",  UNDEFINED_DDR_MEMORY_BASE);
			writeReg("UNDEFINED_DDR_MEMORY_BASE", UNDEFINED_DDR_MEMORY_BASE, 0xDEADBEEF);


	print("\n\n   Reading Sub-System Peripherals\n\r");

		print("\n\t APU Peripherals\n\r");
				readReg("APU_UART0_NS_START", APU_UART0_NS_START);
				readReg("APU_SWDT0_NS_START", APU_SWDT0_NS_START);
				readReg("APU_TTC0_NS_START",  APU_TTC0_NS_START);

			print("\n\t RPU Peripherals\n\r");
				readReg("RPU_UART1_S_START", RPU_UART1_S_START);
				readReg("RPU_SWDT1_S_START", RPU_SWDT1_S_START);
				readReg("RPU_TTC1_S_START",  RPU_TTC1_S_START);
				readReg("RPU_I2C1_S_START",  RPU_I2C1_S_START);

			print("\n\t Shared Peripherals\n\r");
				readReg("SHARED_GPIO_NS_START", SHARED_GPIO_NS_START);


	print("\n\n---Fault Injection Test Complete---\n\n\n\r");

// Done

    cleanup_platform();
    return 0;
}

void readReg(char registerName[30], u32 registerAddress)
{
	u32 regVal;

	exceptionDetected = false;
		xil_printf("\t\t Reading %-30s ... ", registerName);
			regVal=Xil_In32(registerAddress);
			print(" ");
			usleep(DELAY_COUNT);
		print(exceptionDetected ? "FAILED!":"PASS!"); print("\n\r");
	exceptionDetected = false;
}

void writeReg(char registerName[30], u32 registerAddress, u32 regVal)
{
	exceptionDetected = false;
	xil_printf("\t\t Writing %-30s ... ", registerName);
			Xil_Out32(registerAddress, regVal);
			print(" ");
			usleep(DELAY_COUNT);
		print(exceptionDetected ? "FAILED!":"PASS!"); print("\n\r");
	exceptionDetected = false;
}


static int SetupInterruptSystem(void)
{
	//Connect the interrupt controller interrupt handler to the hardware
	//interrupt handling logic in the ARM processor.

	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_SYNC_INT,
			(Xil_ExceptionHandler) SAbort_SyncIntHandler,
			XIL_EXCEPTION_ID_SYNC_INT);

	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_SERROR_ABORT_INT,
			(Xil_ExceptionHandler) SAbort_SErrorAbortIntHandler,
			XIL_EXCEPTION_ID_SERROR_ABORT_INT);


	//Enable interrupts in the ARM
	Xil_ExceptionEnable();

	return 0;
}


void SAbort_SyncIntHandler(int Data)
{
	exceptionDetected = true;
	usleep(DELAY_COUNT);

	//update the return address to prevent returning to the same offending read transaction
	__asm__ __volatile ("mrs x1, ELR_EL3");
	__asm__ __volatile ("add x1, x1, #4");
	__asm__ __volatile ("msr ELR_EL3, x1");
return ;
}

void SAbort_SErrorAbortIntHandler(int Data)
{
	exceptionDetected = true;
	usleep(DELAY_COUNT);
return ;
}





