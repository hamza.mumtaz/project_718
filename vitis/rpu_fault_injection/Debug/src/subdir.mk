################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/platform.c \
../src/rpuFaultInjection.c 

OBJS += \
./src/platform.o \
./src/rpuFaultInjection.o 

C_DEPS += \
./src/platform.d \
./src/rpuFaultInjection.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM R5 gcc compiler'
	armr5-none-eabi-gcc -DARMR5 -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -mcpu=cortex-r5 -mfloat-abi=hard  -mfpu=vfpv3-d16 -IC:/Hamza/Mixed_Criticality/workspace/vitis/Base_Zynq_MPSoC_wrapper_3/export/Base_Zynq_MPSoC_wrapper_3/sw/Base_Zynq_MPSoC_wrapper_3/standalone_domain/bspinclude/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


