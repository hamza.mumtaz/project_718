-= SD card boot image =-

Platform: Base_Zynq_MPSoC_wrapper_3
Application: Base_Zynq_MPSoC_wrapper

1. Copy the contents of this directory to an SD card
2. Set boot mode to SD
3. Insert SD card and turn board on
