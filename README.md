The repo contains all the projects created for the ECE: 718 Project

Directory Discription:
1. boot_images
   It contains boot images for the zcu102 board.

2. ps_isolation
   It contains Vivado hardware project of ZCU102.
 
3. sdk
   It contains FreeRTOS project with custom hardware.
   
4. Vitis
    It contains all the other projects that are required in 718 project.
	Such as FSBL, PMU_Firmware, APU fault injection and RPU fault injection.